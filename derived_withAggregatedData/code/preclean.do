cap log close
set linesize 250
version 12

clear all
set matsize 5000
set more off, permanently

set seed 66666666
set sortseed 66666666
set scheme s2mono

adopath + ..\external


program main
	preclean 
end

 
program preclean 
	import excel "..\external\门诊收入综合报表v2.xlsx", sheet("Sheet1_English") firstrow clear
	destring year month total_outpatient_fee outpatient_inspection_fee, replace force
	g timeLine = (year-2016)*12 + month
	save ../output/profit_category_Tline.dta, replace
	
	import excel "..\external\门诊复诊预约率.xlsx", sheet("department_English") firstrow clear
	g timeLine = (year-2016)*12 + month
	save ../output/patient_revisit_Tline.dta, replace	

	import excel "..\external\门诊均费报表.xlsx", sheet("Sheet1_English") firstrow clear
	g timeLine = (year-2016)*12 + month
	save ../output/profit_percapita_category_Tline.dta, replace		
	
	import excel "..\external\门诊处方报表.xlsx", sheet("unit_prescription_English") firstrow clear
	g timeLine = (year-2016)*12 + month
	save ../output/prescription_category_Tline.dta, replace			

	import excel "..\external\门诊手术例数报表.xlsx", sheet("unit_total_English") firstrow clear
	g timeLine = (year-2016)*12 + month
	save ../output/surgery_category_Tline.dta, replace	
	
	import excel "..\external\门诊开药明细表.xlsx", sheet("Sheet1_English") firstrow clear
	g timeLine = (year-2016)*12 + month
	save ../output/drug_category_Tline.dta, replace			

	import excel "..\external\department_subdepartment_translation.xlsx", sheet("match item") firstrow clear
	save ../output/department_subdepartment.dta, replace				
end


main
