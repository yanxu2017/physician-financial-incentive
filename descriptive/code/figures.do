cap log close
set linesize 250
version 12

clear all
set matsize 5000
set more off, permanently
 
set seed 66666666
set sortseed 66666666
set scheme s2mono

adopath + ..\external


program main
	
	figures_patient_byDoctortype
	patient_visittime_byDoc
	prescription_type
	descriptive_dermatology
	figures_temporary
end


program figures_patient_byDoctortype
	use ../temp/totalpatient_target_department, clear
	local list "dermatology ear tooth rectum chest"
	local list2 "general associate chief"
	local list3 "patientPerDoc uniquePatientPerDoc"
	local list4 "all self ins"
	replace uniquePatientPerDoc =uniquePatientPerDoc/1000
	replace uniquePatientPerDoc1 = uniquePatientPerDoc1/1000
	replace patientPerDoc = patientPerDoc/1000
	replace patientPerDoc1 = patientPerDoc1/1000

	foreach v in `list' {
		di "`v'"
		
		preserve
			keep if department == "`v'"
			foreach k in `list2' {
			
				forvalues w =1(1)2 {
					if `w' == 1 {
						local m "patientPerDoc"
					}
					else{
						local m "uniquePatientPerDoc"
					}
					su `m' if doctor_level == "`k'"
					local ymax = round(r(max),0.1)
					local ymin = round(r(min)/3,0.1)
					local a= round((`ymax'-`ymin')/3,0.1)	
					
					tw ///
						(scatter `m' d if d~=0 & d>-7 & d<7 & doctor_level == "`k'", mcolor(gs10) msize(tiny) ylabel(`ymin'(`a')`ymax')) ///
						(lpoly `m' d if d<0 & d>-7 & doctor_level == "`k'", bwidth(2) lcolor(edkblue) lpattern(solid) fcolor(edkblue) fintensity(inten20) alcolor(white)) ///
						(lpoly `m' d if d>0 & d<7 & doctor_level == "`k'", bwidth(2) lcolor(edkblue) lpattern(solid) fcolor(edkblue) fintensity(inten20) alcolor(white)), ///
						xline(0) legend(off) title(" ") xtitle(" ")  ytitle("no. patients (K)") ///
						graphregion(fcolor(white)) name(all_`k'`w') 				
					tw ///
						(scatter `m' d if d~=0 & d>-7 & d<7 & doctor_level == "`k'", mcolor(gs10) msize(tiny)) ///
						(lpoly `m' d if d<0 & d>-7 & doctor_level == "`k'", bwidth(2) lcolor(edkblue) lpattern(solid) fcolor(edkblue) fintensity(inten20) alcolor(white)) ///
						(lpoly `m' d if d>0 & d<7 & doctor_level == "`k'", bwidth(2) lcolor(edkblue) lpattern(solid) fcolor(edkblue) fintensity(inten20) alcolor(white)), ///
						xline(0) legend(off) title(" ") xtitle(" ")  ytitle("no. patients (K)") ///
						graphregion(fcolor(white)) name(all1_`k'`w') 					

					tw ///
						(scatter `m'1 d if d~=0 & d>-7 & d<7 & doctor_level == "`k'" & insurance ==0, mcolor(gs10) msize(tiny) ylabel(`ymin'(`a')`ymax')) ///
						(lpoly `m'1 d if d<0 & d>-7 & doctor_level == "`k'" & insurance ==0, bwidth(2) lcolor(edkblue) lpattern(solid) fcolor(edkblue) fintensity(inten20) alcolor(white)) ///
						(lpoly `m'1 d if d>0 & d<7 & doctor_level == "`k'" & insurance ==0, bwidth(2) lcolor(edkblue) lpattern(solid) fcolor(edkblue) fintensity(inten20) alcolor(white)), ///
						xline(0) legend(off) title(" ") xtitle(" ")  ytitle("no. patients (K)") ///
						graphregion(fcolor(white)) name(self_`k'`w') 
					

					tw ///
						(scatter `m'1 d if d~=0 & d>-7 & d<7 & doctor_level == "`k'" & insurance ==1, mcolor(gs10) msize(tiny) ylabel(`ymin'(`a')`ymax')) ///
						(lpoly `m'1 d if d<0 & d>-7 & doctor_level == "`k'" & insurance ==1, bwidth(2) lcolor(edkblue) lpattern(solid) fcolor(edkblue) fintensity(inten20) alcolor(white)) ///
						(lpoly `m'1 d if d>0 & d<7 & doctor_level == "`k'" & insurance ==1, bwidth(2) lcolor(edkblue) lpattern(solid) fcolor(edkblue) fintensity(inten20) alcolor(white)), ///
						xline(0) legend(off) title(" ") xtitle(" ")  ytitle("no. patients (K)") ///
						graphregion(fcolor(white)) name(ins_`k'`w') 			
				}
			
			}
			
		restore
		local graph ""
		local graph1 ""
		foreach k in `list4' {
			local graph "`graph' `k'_general1 `k'_associate1 `k'_chief1"
			local graph1 "`graph1' `k'_general2 `k'_associate2 `k'_chief2"
		}
		graph combine `graph', title("TotalPatient per month--`v'", size(small))	
		graph export ../output/figures/TotalPatient_`v'.eps, replace
		graph combine `graph1', title("uniqueTotalPatient per month--`v'", size(small))	
		graph export ../output/figures/uniqueTotalPatient_`v'.eps, replace
		* pool both selfpay and insurance together
		graph combine all1_general2 all1_associate2 all1_chief2 all1_general1 all1_associate1 all1_chief1, title("uniqueTotalPatients/TotalPatients per month--`v'", size(small)) row(2)
		graph export ../output/figures/totalPatient_`v'_pool.eps, replace
		graph drop _all
	}
	
end


program patient_visittime_byDoc
	use ../temp/totalpatient_target_department, clear
	local list "dermatology ear tooth rectum chest"
	bys department doctor_level year month uniqueid: g n = _n
	keep if n == 1
	foreach v in `list'{
		local graphlist ""
		forvalues m = 1(1)3{
			preserve
				keep if department == "`v'"
				if `m' == 2 {
					keep if year == 2018 & month < 7
				}
				if `m' == 3 {
					keep if year == 2018 & month > 7
				}			
				twoway (hist visittimes if doctor_level == "general", frac lcolor(green) fcolor(none)) ///
			   (hist visittimes if doctor_level == "associate", frac lcolor(gs12) fcolor(none)) ///
			   (hist visittimes if doctor_level == "chief", frac fcolor(none) lcolor(red)), legend(order(1 "general" 2 "associate" 3 "chief") ) xtitle("`v'") name(`v'`m')
			   local graphlist "`graphlist' `v'`m'"
			restore				
		}
		graph combine `graphlist', graphregion(fcolor(white))
		graph export ../output/figures/patient_visittime_`v'.eps, replace
		graph drop _all		
	}
end


program prescription_type	
	use ../temp/totalpatient_target_department.dta, clear

	replace numPatientDay = numPatientDay/numDoctorByLevelDay
	replace profitTT = profitTT/numDoctorByLevel 
	drop if profitTT>1500000
	save ../temp/target_department_v2_preplot.dta, replace
	
	forvalues m = 1(1)3 {
		use ../temp/target_department_v2_preplot.dta, clear
			if `m' == 1 { 
			local list "dermatology ear rectum chest tooth"
			foreach k in `list' {
				preserve
					keep if department == "`k'"
					twoway (hist profitTT if doctor_level == "general", frac lcolor(green) fcolor(none)) ///
						   (hist profitTT if doctor_level == "associate", frac lcolor(gs12) fcolor(none)) ///
						   (hist profitTT if doctor_level == "chief", frac fcolor(none) lcolor(red)), legend(order(1 "general" 2 "associate" 3 "chief") ) xtitle("`k'") name(`k'`m')
				restore
				preserve
					keep if department == "`k'"
					twoway (hist uniquePatientPerDoc if doctor_level == "general", frac lcolor(green) fcolor(none)) ///
						   (hist uniquePatientPerDoc if doctor_level == "associate", frac lcolor(gs12) fcolor(none)) ///
						   (hist uniquePatientPerDoc if doctor_level == "chief", frac fcolor(none) lcolor(red)), legend(order(1 "general" 2 "associate" 3 "chief") ) xtitle("`k'") name(`k'`m'numPatient)
				restore
				preserve
					keep if department == "`k'"
					twoway (hist numPatientDay if doctor_level == "general", frac lcolor(green) fcolor(none)) ///
						   (hist numPatientDay if doctor_level == "associate", frac lcolor(gs12) fcolor(none)) ///
						   (hist numPatientDay if doctor_level == "chief", frac fcolor(none) lcolor(red)), legend(order(1 "general" 2 "associate" 3 "chief") ) xtitle("`k'") name(`k'`m'numPatientDay)
				restore			
			}	
			}
			if `m' == 2 { 
			keep if year == 2018 & month < 7 
			local list "dermatology ear rectum chest tooth"
			foreach k in `list' {
				preserve
					keep if department == "`k'"
					twoway (hist profitTT if doctor_level == "general", frac lcolor(green) fcolor(none)) ///
						   (hist profitTT if doctor_level == "associate", frac lcolor(gs12) fcolor(none)) ///
						   (hist profitTT if doctor_level == "chief", frac fcolor(none) lcolor(red)), legend(order(1 "general" 2 "associate" 3 "chief") ) xtitle("`k'") name(`k'`m')
				restore
				preserve
					keep if department == "`k'"
					twoway (hist uniquePatientPerDoc if doctor_level == "general", frac lcolor(green) fcolor(none)) ///
						   (hist uniquePatientPerDoc if doctor_level == "associate", frac lcolor(gs12) fcolor(none)) ///
						   (hist uniquePatientPerDoc if doctor_level == "chief", frac fcolor(none) lcolor(red)), legend(order(1 "general" 2 "associate" 3 "chief") ) xtitle("`k'") name(`k'`m'numPatient)
				restore
				preserve
					keep if department == "`k'"
					twoway (hist numPatientDay if doctor_level == "general", frac lcolor(green) fcolor(none)) ///
						   (hist numPatientDay if doctor_level == "associate", frac lcolor(gs12) fcolor(none)) ///
						   (hist numPatientDay if doctor_level == "chief", frac fcolor(none) lcolor(red)), legend(order(1 "general" 2 "associate" 3 "chief") ) xtitle("`k'") name(`k'`m'numPatientDay)
				restore			
			}
			}
			if `m' == 3 {
			keep if year == 2018 & month > 7 
			local list "dermatology ear rectum chest tooth"
			foreach k in `list' {
				preserve
					keep if department == "`k'"
					twoway (hist profitTT if doctor_level == "general", frac lcolor(green) fcolor(none)) ///
						   (hist profitTT if doctor_level == "associate", frac lcolor(gs12) fcolor(none)) ///
						   (hist profitTT if doctor_level == "chief", frac fcolor(none) lcolor(red)), legend(order(1 "general" 2 "associate" 3 "chief") ) xtitle("`k'") name(`k'`m')
				restore
				preserve
					keep if department == "`k'"
					twoway (hist uniquePatientPerDoc if doctor_level == "general", frac lcolor(green) fcolor(none)) ///
						   (hist uniquePatientPerDoc if doctor_level == "associate", frac lcolor(gs12) fcolor(none)) ///
						   (hist uniquePatientPerDoc if doctor_level == "chief", frac fcolor(none) lcolor(red)), legend(order(1 "general" 2 "associate" 3 "chief") ) xtitle("`k'") name(`k'`m'numPatient)
				restore
				preserve
					keep if department == "`k'"
					twoway (hist numPatientDay if doctor_level == "general", frac lcolor(green) fcolor(none)) ///
						   (hist numPatientDay if doctor_level == "associate", frac lcolor(gs12) fcolor(none)) ///
						   (hist numPatientDay if doctor_level == "chief", frac fcolor(none) lcolor(red)), legend(order(1 "general" 2 "associate" 3 "chief") ) xtitle("`k'") name(`k'`m'numPatientDay)
				restore			
			}	
			}
		}
		local list "dermatology ear rectum chest tooth"
			foreach k in `list' {
				graph combine `k'1 `k'2 `k'3
				graph export ../output/figures/fee_`k'.eps, replace
				graph combine `k'1numPatient `k'2numPatient `k'3numPatient 
				graph export ../output/figures/numPatient_`k'.eps, replace
				graph combine `k'1numPatientDay `k'2numPatientDay `k'3numPatientDay
				graph export ../output/figures/numPatientDay_`k'.eps, replace
		}
 
	graph drop _all
end


program descriptive_dermatology
	
	use ../temp/preclean_dermatology_withSeverity.dta, clear
	keep if visitSequence == 1
	local list "addedseverity topseverity"	
	
	foreach k in `list'{
		preserve
			keep if d > -7 & d < 0
			bys doctor_level `k': g patientnumber_before = _N
			bys doctor_level `k': g n = _n
			keep if n == 1
			keep doctor_level `k' patientnumber_before
			save ../temp/`k'_numberbefore, replace
		restore
		
		preserve
			keep if d > 0 & d < 7
			bys doctor_level `k': g patientnumber_aft = _N
			bys doctor_level `k': g n = _n
			keep if n == 1
			keep doctor_level `k' patientnumber_aft
			mmerge doctor_level `k' using ../temp/`k'_numberbefore, type(1:1) ukeep(patientnumber_before) unmatch(both)
			drop _merge
			
			if "`k'" == "addedseverity"{
				graph bar patientnumber_before patientnumber_aft, over(addedseverity) over(doctor_level) bargap(10) intensity(*0.75) ///
				bar(1, color(edkblue)) bar(2, color(eltblue)) legend(order(1 "half year before policy" 2 "half year after policy")) ytitle("No. of patients") title(" ") name(`k'1)		
			}
			else{
				graph bar patientnumber_before patientnumber_aft , over(topseverity) over(doctor_level) bargap(10) intensity(*0.75) ///
				bar(1, color(edkblue)) bar(2, color(eltblue)) legend(order(1 "half year before policy" 2 "half year after policy")) ytitle("No. of patients") title(" ") name(`k'1)										
			}
			graph export ../output/figures/`k'_bar_dermatology.eps, replace			
			graph drop _all
		restore		
	}	
	
	local typelist "addedseverity topseverity"	
	foreach m in `typelist' {	
		bys doctor_level doctorid year month: egen ave`m' = mean(`m')
		local list "general associate chief"
		local graphlist ""
		foreach k in `list'{
			graph box ave`m' if d>=-7 & d <=7 & doctor_level == "`k'", nooutsides over(d) box(1, color("51 102 204")) ytitle("severity") ///
				title(`k') name(`k')
			local graphlist "`graphlist' `k'"
		}
		graph combine `graphlist', graphregion(fcolor(white))
		graph export ../output/figures/`m'_box.eps, replace
		graph drop _all	
	}
	
end


program figures_temporary

	use ../temp/preclean_dermatology_withSeverity.dta, clear
	bys uniqueid: egen totalCost = total(totalcost)
// 	*keep the patient's first arrival
// 	keep if visitSequence == 1
	g costFirst = 1 if totalcost>150
	replace costFirst = 0 if costFirst == .	
	g costTotal = 1 if totalCost>400
	replace costTotal = 0 if costTotal == .
	
	g doctorLevel = 1 if doctor_level == "general"
	replace doctorLevel = 2 if doctor_level == "associate"
	replace doctorLevel = 3 if doctor_level == "chief"
	
	local list "costFirst costTotal"	
	
	foreach k in `list'{
		preserve
			keep if d > -7 & d < 0
			bys doctor_level `k': g patientnumber_before = _N
			bys doctor_level `k': g n = _n
			keep if n == 1
			keep doctor_level `k' patientnumber_before
			save ../temp/`k'_numberbefore, replace
		restore
		
		preserve
			keep if d > 0 & d < 7
			bys doctor_level `k': g patientnumber_aft = _N
			bys doctor_level `k': g n = _n
			keep if n == 1
			keep doctor_level `k' patientnumber_aft doctorLevel
			mmerge doctor_level `k' using ../temp/`k'_numberbefore, type(1:1) ukeep(patientnumber_before) unmatch(both)
			drop _merge
			
			forvalues z = 1(1)3 {
				graph bar patientnumber_before patientnumber_aft if doctorLevel == `z', over(`k') bargap(10) intensity(*0.75) exclude0 ysc(r(2000,4000)) yla(2000(1500)5000) ///
					bar(1, color(edkblue)) bar(2, color(eltblue)) legend(order(1 "half yr before policy" 2 "half yr after policy")) ytitle("") ///
					title("no. patients -- doctor level `z'") name(graph`z'`k')				
			}
			graph combine graph1`k' graph2`k' graph3`k' 
			graph export ../output/figures/`k'_bar_dermatology.eps, replace			
			graph drop _all
		restore		
	}	

end

main


