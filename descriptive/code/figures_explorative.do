cap log close
set linesize 250
version 12

clear all
set matsize 5000
set more off, permanently
 
set seed 66666666
set sortseed 66666666
set scheme s2mono

adopath + ..\external


program main
	mean_cost
	*intended to check (1) ratio of registration fee versus total expenditure (hopefully it is large enough), (2) check whether expenditure significantly change at a certain point in time
	age_distribution
	*intended to check whether with-insurance patients are from a totally different demographic group
	utilization_rate
end


program mean_cost
	use ../temp/totalpatient_target_department, clear
	local list "dermatology ear rectum chest tooth"
	bys department doctor_level year month: egen totalCostbyDoc = sum(totalcost)
	bys department doctor_level year month: g n = _n
	keep if n == 1
	g average_cost = totalCostbyDoc/uniqueNumPatient
	drop n

	foreach k in `list'{
		preserve
		keep if department == "`k'"
			tw ///
		(line average_cost d if d<=12 & d >=-12 & doctor_level == "general", lcolor(edkblue) lpattern(solid) fcolor(edkblue) fintensity(inten20) alcolor(white)) ///
		(line average_cost d if d<=12 & d >=-12 & doctor_level == "associate", lcolor(dkgreen) lpattern(solid) fcolor(dkgreen) fintensity(inten20) alcolor(white)) ///
		(line average_cost d if d<=12 & d >=-12 & doctor_level == "chief", lcolor(dkorange) lpattern(solid) fcolor(dkorange) fintensity(inten20) alcolor(white)), ///
		xline(0) title("`k'") ///
		legend(label(1 "general") label(2 "associate") label(3 "chief") label(4 "")) ///
		graphregion(fcolor(white)) name(`k') 
		graph export ../output/figures/explorative_`k'_AVcost.eps, replace	
		restore
	}
end


program age_distribution
	use ../temp/totalpatient_target_department, clear
	mmerge prescripID dataSetYr using ../external/prescriptionid_dictionary, type(1:1) ukeep(inneridkey) unmatch(master)
	drop _merge
	mmerge inneridkey using ../external/cardinfo_rev, type(n:1) ukeep(gender birthYr birthMon birthDay) unmatch(master)
	drop _merge
	g age = year -birthYr + 1
	bys department uniqueid year insurance: g n = _n
	keep if n == 1
	local list "dermatology ear chest"
	foreach k in `list'{
		preserve
		keep if department == "`k'"
		local graphlist ""
		forvalue v = 2016(1)2019{
			tw ///
			(histogram age if dataSetYr == `v' & insurance == 0 & age >0 & age < 100, lcolor(edkblue) lpattern(solid) fcolor(none) fintensity(inten20) alcolor(edkblue) xlabel(0(10)100)) ///
			(histogram age if dataSetYr == `v' & insurance == 1 & age >0 & age < 100, lcolor(dkgreen) lpattern(solid) fcolor(none) fintensity(inten20) alcolor(red)), ///
			legend(label(1 "`v'-selfpay") label(2 "`v'-insurance")) ///
			title("`k'-`v'") ///
			graphregion(fcolor(white)) name(a`v')
			local graphlist "`graphlist' a`v'"
		}
		graph combine `graphlist', graphregion(fcolor(white))
		graph export ../output/figures/explorative_`k'_age_distribution.eps, replace
		graph drop _all
		restore
	}
end


program utilization_rate
	*utilization rates
	
	use ../temp/dermatology, clear
	drop if doctorname == "刘远"
	*this doctor only provided outpatient service from Jan 2019
	keep if d > -7 & d < 7
	
	local list "bydoctor bylevel"
	foreach m in `list' {
		*m==bydoctor, calculate the utilization rate by each doctor and collapse them into the graph
		*m==bylevel, do not differenciate doctors under each doctor_level
		local doctorlevellist "general associate chief"
		local graph ""
		preserve
		if "`m'" == "bydoctor" {
			bys doctor_level doctorname year month: g patientnumber = _N
			bys doctor_level doctorname year month: g n = _n
			keep if n == 1
			drop n
			
			mmerge doctorid using ../temp/dermatology_onshift_doctorlevel, type(n:1) ukeep(quota) unmatch(master)
			drop _merge
			g utilization_rate = patientnumber/quota*100
			bys doctor_level d: egen rate = mean(utilization_rate)			
		}
		else {
			bys doctor_level year month: g patientnumber = _N
			bys doctor_level year month: g n = _n
			keep if n == 1
			drop n
			mmerge doctor_level using ../temp/dermatology_onshift_doctorlevel, type(n:n) ukeep(quota_byLevel) unmatch(master)
			g rate = patientnumber/quota_byLevel*100				
		}
		
		foreach k in `doctorlevellist'{
			tw ///
				(scatter rate d if d~=0 & d>-7 & d<7 & doctor_level == "`k'", mcolor(gs10) msize(tiny)) ///
				(lpoly rate d if d<0 & d>-7 & doctor_level == "`k'", bwidth(2) lcolor(edkblue) lpattern(solid) fcolor(edkblue) fintensity(inten20) alcolor(white)) ///
				(lpoly rate d if d>0 & d<7 & doctor_level == "`k'", bwidth(2) lcolor(edkblue) lpattern(solid) fcolor(edkblue) fintensity(inten20) alcolor(white)), ///
				xline(0) legend(off) title("utilization rate of `k' doctor's time slots") xtitle(" ")  ytitle("percentage") ///
				graphregion(fcolor(white)) name(`k'`m') 	
				local graph "`graph' `k'`m'"
		
		}
		graph combine `graph', graphregion(fcolor(white))
		graph export ../output/figures/`m'_utilization_rate_dermatology.eps, replace
		graph drop _all		
		restore
	}
end


main


