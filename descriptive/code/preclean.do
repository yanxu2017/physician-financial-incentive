cap log close
set linesize 250
version 12

clear all
set matsize 5000
set more off, permanently
set seed 66666666
set sortseed 66666666
set scheme s2mono

adopath + ..\external


program main
	preclean_doctor_id
	selectDepartment
	preclean 
	preclean_dermatology_diagnosis
end

program preclean_doctor_id
	import excel "..\external\doctor_level_list.xlsx", sheet("Sheet") firstrow clear
	drop if year == .
	destring doctorid, replace
	save ../temp/doctor_level.dta, replace
end


program selectDepartment	
	import excel "..\external\人管库201909.xlsx", sheet("基础数据 ") firstrow clear
	keep 所属科室 姓名 参加工作时间 学历技能现职称 学历技能现职称取得时间 获得时间 取得时间2 取得时间3 取得时间4 取得时间5 取得时间6 
	split 参加工作时间,p("-")
	split 学历技能现职称取得时间,p("-")
	split 获得时间,p("-")
	split 取得时间2,p("-")
	split 取得时间3,p("-")
	split 取得时间4,p("-")
	split 取得时间5,p("-")
	split 取得时间6,p("-")
	rename (参加工作时间1 参加工作时间2) (joinWorkYr joinWorkMonth)
	rename (学历技能现职称取得时间1 学历技能现职称取得时间2) (titleYr titleMonth)
	drop 参加工作时间* 学历技能现职称取得时间*
	destring joinWorkYr joinWorkMonth titleYr titleMonth 获得时间1 取得时间21 取得时间31 取得时间41 取得时间51 取得时间61, replace
	bys 所属科室: g numStaffTotal = _N
	keep if 学历技能现职称 == "主任中医师"|学历技能现职称 == "主任医师"|学历技能现职称 == "主治中医师"|学历技能现职称 == "主治医师"|学历技能现职称 == "副主任中医师"|学历技能现职称 == "副主任医师" 
	g isLevel1 = 1 if 学历技能现职称 == "主任中医师"|学历技能现职称 == "主任医师"
	g isLevel2 = 1 if 学历技能现职称 == "副主任中医师"|学历技能现职称 == "副主任医师" 
	g isLevel3 = 1 if 学历技能现职称 == "主治中医师"|学历技能现职称 == "主治医师"
	bys 所属科室: g numStaffWithTitleTotal = _N
	bys 所属科室: egen numTitle = nvals(学历技能现职称)
	g tilteChangeAfterPolicy = 1 if titleYr == 2018 & titleMonth>7
	g tilteChangeAfterPolicyLevel12 = 1 if titleYr == 2018 & titleMonth>7 & (学历技能现职称 == "主任中医师"|学历技能现职称 == "主任医师"|学历技能现职称 == "副主任中医师"|学历技能现职称 == "副主任医师") 
	rename 所属科室 department
	collapse (sum) tilteChangeAfterPolicy tilteChangeAfterPolicyLevel12 isLevel* (mean) numStaffWithTitleTotal numTitle numStaffTotal joinWorkYr titleYr, by(department)
	g percentTitleChange = tilteChangeAfterPolicy/numStaffWithTitleTotal
	g percentTitleChangeLevel12 = tilteChangeAfterPolicyLevel12/numStaffWithTitleTotal
	save ../temp/departmentStaffing.dta, replace
	
	use ../external/outpatient_prescription.dta, clear	
	keep if prescriptionYr>2015
	g index = 1
	collapse (sum) numPrescription = index profit = cost, by(department prescriptionYr)
	drop if department == "" | department == "NULL"
	bys department: g N = _N
	save ../temp/departmentSize.dta, replace
	
	use ../temp/departmentSize.dta, clear
	drop if N < 4
	collapse (sum) numPrescription profit, by(department)
	mmerge department using ../temp/departmentStaffing.dta, type(1:1) 
	gsort _m -profit
	save ../temp/selectDepartment.dta, replace
end

program preclean
	import excel using ../external/staff_dictionary.xlsx, sheet("doctoridlist") firstrow clear
	save ../temp/doctoridlist, replace
	
	use ../external/outpatient_prescription, clear
	keep if dataSetYr >2015
	keep if department == "皮肤性病科" | department == "耳鼻喉科、耳鼻喉肿瘤外科" | department == "口腔门诊" | department == "结直肠肛门外科、结直肠肿瘤外科" | department == "胸外科、胸部肿瘤外科"
	mmerge prescripID dataSetYr using ../external/prescriptionid_dictionary, type(1:1) ukeep(patientid uniqueid) unmatch(master)
	drop _merge

	*cleanup the missing doctorid
	replace doctorname = "王洪芹" if doctorname =="王洪琴"
	rename doctorid_final_more doctorid
    mmerge doctorname using ../temp/doctoridlist.dta, type(n:1) ukeep(doctorid) update unmatch(master)	

	*drop the unsuccessful prescription
	drop if status == 1
	bys patientid prescriptionYr prescriptionMo prescriptionDay doctorid: egen totalcost = sum(cost)
	duplicates drop patientid prescriptionYr prescriptionMo prescriptionDay doctorid,force
	*keep unique patient and exclude prescriptions under one patient per visit
	rename (prescriptionYr prescriptionMo) (year month)	
	mmerge year month doctorid using ../temp/doctor_level, type(n:1) ukeep(doctor_level) unmatch(master)
	g len = strlen(patientid)
// 	mmerge department using ../temp/departmentStaffing, type(n:1) unmatch(master)
// 	drop _merge
	keep if len == 8 | len == 19
	mmerge uniqueid using ../external/insurancelist, type(n:1) ukeep(insurance) unmatch(master)
	drop _merge patientid

	replace department = "dermatology" if department == "皮肤性病科"
	replace department = "ear" if department == "耳鼻喉科、耳鼻喉肿瘤外科"
	replace department = "tooth" if department == "口腔门诊"
	replace department = "rectum" if department == "结直肠肛门外科、结直肠肿瘤外科"
	replace department = "chest" if department == "胸外科、胸部肿瘤外科"
	replace doctor_level = "general" if doctor_level == "主治医师"
	replace doctor_level = "associate" if doctor_level == "副主任医师"	
	replace doctor_level = "chief" if doctor_level == "主任医师"
	g d = (year - 2018)*12 + month - 7
	bys department doctor_level uniqueid year month prescriptionDay: g n =_n
	keep if n == 1
	drop n
	drop if missing(doctor_level)
	drop len cost departmentold
	save ../temp/totalpatient_target_department_test, replace

	use ../temp/totalpatient_target_department_test,clear
	bys department doctor_level year month: egen numDoctorByLevel = nvals(doctorid)
	bys department doctor_level year month prescriptionDay: egen numDoctorByLevelDay = nvals(doctorid)
	bys department doctor_level year month: egen profitTT = total(totalcost)
	bys department doctor_level year month prescriptionDay: egen numPatientDay = nvals(uniqueid)

	

	*uniqueNumPatient: not distinguish the selfpay and insurance patient, count the visit time as 1 if the same patient revisit the doctor within one month
	*uniqueNumPatient1: distinguish the selfpay and insurance patient, count the visit time as 1 if the same patient revisit the doctor within one month
	*numPatient: not distinguish the selfpay and insurance patient, count the real times for one patient with many visits
	*numPatient1: distinguish the selfpay and insurance patient, count the real times for one patient with many visits
	bys department doctor_level year month: egen uniqueNumPatient = nvals(uniqueid)
	bys department doctor_level insurance year month: egen uniqueNumPatient1 = nvals(uniqueid)
	bys department doctor_level year month: g numPatient = _N
	bys department doctor_level insurance year month: g numPatient1 = _N
	g uniquePatientPerDoc = uniqueNumPatient/numDoctorByLevel
	g uniquePatientPerDoc1 = uniqueNumPatient1/numDoctorByLevel
	g patientPerDoc = numPatient/numDoctorByLevel
	g patientPerDoc1 = numPatient1/numDoctorByLevel
	g feePerPatient = profitTT/uniqueNumPatient
	*visittimes: total visit times in one month
	bys department doctor_level year month uniqueid: g visittimes = _N
	save ../temp/totalpatient_target_department, replace
end

program preclean_dermatology_diagnosis

	use ../temp/totalpatient_target_department, clear
	keep if department == "dermatology"
	mmerge prescripID dataSetYr using ../external/diagnosis_dermatology, type(1:1) ukeep(noise severity*) unmatch(master)
	drop _merge
	save ../temp/dermatology, replace
	
	use ../temp/dermatology, clear
	drop if noise == 0
	*drop the noise diagnosis record
	drop if missing(doctor_level)
	forvalues k = 1(1)6{
		g severityscore`k' = 0
		replace severityscore`k' = 1 if severity`k' == "low"
		replace severityscore`k' = 2 if severity`k' == "moderate"
		replace severityscore`k' = 3 if severity`k' == "high"
	}	
	egen topseverity = rmax(severityscore1-severityscore6)
	g addedseverity = severityscore1 + severityscore2+ severityscore3 + severityscore4 +severityscore5 + severityscore6
	g timeamount = year*10000 + month*100 + prescriptionDay
	sort uniqueid timeamount
	bys uniqueid: g visitSequence = _n
	save ../temp/preclean_dermatology_withSeverity.dta, replace
	
end

main

/*calculate waiting time
	/*generate the duration of the doctor visit
	*/

	use ../output/outpatient_prescription, clear
	drop if missing(prescripID)
	*dropped 25085 obs. These missing values only contains doctorid and type. The other variables are missing. The reason is that we 
	*clean up the prescription fisrt and drop some format error obs. Then we use 门诊final_more to complement doctorid and type information. This is how the missing values generated.
	mmerge prescripID dataSetYr using ../output/prescriptionid_dictionary, type(1:1) ukeep(inneridkey) unmatch(master)
	*find related inneridkey for future match
	duplicates drop doctorid inneridkey prescriptionYr prescriptionMo prescriptionDay prescriptionHr prescriptionMinute, force
	*one patient may have many prescription records during one doctor visit. keep only one prescription record for each visit
	
	*genrate waitingtime
	rename doctorid doctorid_temp
	decode doctorid_temp, g(doctorid)
	drop doctorid_temp
	g registrationYr = prescriptionYr
	g registrationMo = prescriptionMo
	g registrationDay = prescriptionDay
    save ../temp/outpatient_prescription_unique, replace
	
	use ../temp/outpatient_registration, clear
	rename doctorid doctorid_temp
	decode doctorid_temp, g(doctorid)
	drop doctorid_temp
	save ../temp/outpatient_registration, replace

	use ../temp/outpatient_prescription_unique, clear
	mmerge doctorid inneridkey registrationYr registrationMo registrationDay using ../temp/outpatient_registration, type (n:n) ukeep(registrationHr registrationMin registrationlevel) unmatch(master)
	keep if _merge == 3
	drop _merge
	*keep the matched observations
	*generate waitingtime
	g waitingtime = prescriptionHr*60 + prescriptionMinute - registrationHr*60 - registrationMin
	des waitingtime
	save ../temp/outpatient_prescription_unique_waiting, replace
	
	*generate meeting time
	use ../temp/outpatient_prescription_unique, clear
	g prescriptiontime = 60*prescriptionHr + prescriptionMinute
	sort doctorid prescriptionYr prescriptionMo prescriptionDay prescriptiontime
	g index = _n
	g meetingtime = prescriptiontime - prescriptiontime[_n-1]
	save ../temp/outpatient_prescription_unique_meeting, replace
	
*/
