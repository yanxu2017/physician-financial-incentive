cap log close
set linesize 250
version 12

clear all
set matsize 5000
set more off, permanently
 
set seed 66666666
set sortseed 66666666
set scheme s2mono

adopath + ..\external


program main
	tables_switching_stateDependence
end


program tables_switching_stateDependence
	*2018 7
	use ../temp/preclean_dermatology_withSeverity.dta, clear
	g visitDate = year*10000 + month*100 + prescriptionDay
	g Tline = (year-2016)*12 + month
	bys uniqueid: egen firstVisitTline = min(Tline)
	g beforeBatch = 1 if firstVisitTline < 31
	replace beforeBatch = 0 if firstVisitTline > 31
	bys uniqueid: g totalNumVisit = _N
	su totalNumVisit, detail
	sort uniqueid year month prescriptionDay
	by uniqueid: g intervalBetweenVisit = Tline[_n+1] - Tline[_n]
	su intervalBetweenVisit, detail
	
	tw hist totalNumVisit if d<0&d>=-12, freq start(1) name(totalNumVisitBefore)
	tw hist totalNumVisit if d>0&d<=12, freq start(1) name(totalNumVisitAfter)
	graph drop _all	
	
	sort uniqueid year month prescriptionDay
	g doctorLevel = 1 if doctor_level == "general"
	replace doctorLevel = 2 if doctor_level == "associate"
	replace doctorLevel = 3 if doctor_level == "chief"
	by uniqueid: g doctorLevelNextVisit = doctorLevel[_n+1]
	g diff = doctorLevelNextVisit - doctorLevel
	g upgrade = 1 if diff > 0 & diff~=.
	replace upgrade = 0 if upgrade == . & diff~=.
	g downgrade = 1 if diff < 0 & diff~=.
	replace downgrade = 0 if downgrade == . & diff~=.
	replace diff = abs(diff)
	reg diff i.doctorLevel##i.beforeBatch, noconstant
	mlogit diff i.doctorLevel##i.beforeBatch, noconstant iterate(100)
	
	reg upgrade i.doctorLevel#i.beforeBatch 
	mlogit upgrade i.doctorLevel#i.beforeBatch uniqueid, iterate(100)
	
	reg downgrade i.doctorLevel#i.beforeBatch 
	mlogit downgrade i.doctorLevel#i.beforeBatch, iterate(100)
	
	sort uniqueid year month prescriptionDay
	by uniqueid: g n = _n
	g switch = 1 if diff > 0 & diff~=.
	replace switch = 0 if diff == 0 & diff~=.
	stset n switch
	sts graph, by(beforeBatch) ci xtitle("no. hospital visits")
	sts graph if n<15, by(beforeBatch) ci xtitle("no. hospital visits")
	sts graph if n<15 & beforeBatch == 0, by(doctorLevel) xtitle("no. hospital visits") name(graph1)
	sts graph if n<15 & beforeBatch == 1, by(doctorLevel) xtitle("no. hospital visits") name(graph2)
	graph combine graph1 graph2
	graph export ../output/KM_switch.eps, replace
	
	
	sort uniqueid year month prescriptionDay
// 	by uniqueid: g n = _n
	g index = 1
	collapse (sum) index, by(beforeBatch n doctorLevel doctor_level)
	graph bar index if beforeBatch == 1 & n<10 & n>1, over(doctorLevel) over(n) ytitle("")
	graph bar index if beforeBatch == 0 & n<10 & n>1, over(doctorLevel) over(n) ytitle("")
	bys beforeBatch n: egen patientTotalByVisitNum = total(index)
	g indexPercentage = index/patientTotalByVisitNum*100
	graph bar indexPercentage if beforeBatch == 1 & n<10 , over(doctorLevel) over(n) ytitle("")
	graph bar indexPercentage if beforeBatch == 0 & n<10 , over(doctorLevel) over(n) ytitle("")	
end
	
	
main


