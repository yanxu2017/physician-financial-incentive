cap log close
set linesize 250
version 12

clear all
set matsize 5000
set more off, permanently

set seed 66666666
set sortseed 66666666
set scheme s2mono

adopath + ..\external


program main
	figures_profit
	figures_patientRevisit
	*figures_profit_perCapita
	*figures_other
end

 
program figures_profit
	use ../external/profit_category_Tline.dta, clear
	rename (total_number_of_patients_in_outp total_outpatient_fee outpatient_regisration_fee outpatient_examination_fee_fee outpatient_Medical_test_fee outpatient_inspection_fee outpatient_treatment_fee outpatient_surgery_fee outpatient_sanitary_supplies_fee outpatient_bed_fee outpatient_nursing_fee total_outpatient_drug_fee outpatient_western_medicine_fee outpatient_Chinese_patent_medici outpatient_Chinese_herbal_medici outpatient_other_fee) ///
		   (total_number_of_patients total_fee regisration_fee examination_fee medical_test_fee inspection_fee treatment_fee surgery_fee sanitary_supplies_fee bed_fee nursing_fee total_drug_fee western_medicine_fee chinese_patent_medici chinese_herbal_medici other_fee)	
    replace regisration_fee = regisration_fee+examination_fee
	local list "total_number_of_patients total_fee regisration_fee medical_test_fee inspection_fee treatment_fee surgery_fee sanitary_supplies_fee bed_fee nursing_fee total_drug_fee western_medicine_fee chinese_patent_medici chinese_herbal_medici other_fee"
	
	preserve
	collapse (sum) `list', by(year month timeLine)
	g d = timeLine - 31
	foreach v in `list' {
		tw ///
		(scatter `v' d, mcolor(gs10) msize(tiny)) ///
		(lpolyci `v' d if d<-7, lcolor(edkblue) lpattern(solid) fcolor(edkblue) fintensity(inten20) alcolor(white)) ///
		(lpolyci `v' d if d>-7 & d<0, lcolor(edkblue) lpattern(solid) fcolor(edkblue) fintensity(inten20) alcolor(white)) ///
		(lpolyci `v' d if d>0, lcolor(edkblue) lpattern(solid) fcolor(edkblue) fintensity(inten20) alcolor(white)), ///
		xline(0) xline(-7) legend(off) title("`v'") ///
		graphregion(fcolor(white)) name(`v'_twoP) 
		graph export ../output/figures/`v'_allDepartmentMean_twoPolicy.eps, replace
		
		tw ///
		(scatter `v' d, mcolor(gs10) msize(tiny)) ///
		(lpolyci `v' d if d<0, lcolor(edkblue) lpattern(solid) fcolor(edkblue) fintensity(inten20) alcolor(white)) ///
		(lpolyci `v' d if d>0, lcolor(edkblue) lpattern(solid) fcolor(edkblue) fintensity(inten20) alcolor(white)), ///
		xline(0) legend(off) title("`v'") ///
		graphregion(fcolor(white)) name(`v') 
		graph export ../output/figures/`v'_allDepartmentMean.eps, replace	
	}
	graph drop _all
	restore

	local list "total_number_of_patients total_fee regisration_fee medical_test_fee inspection_fee treatment_fee surgery_fee sanitary_supplies_fee bed_fee nursing_fee total_drug_fee western_medicine_fee chinese_patent_medici chinese_herbal_medici other_fee"
	collapse (sum) `list', by(year month timeLine department_ID)
	g d = timeLine - 31
	mmerge department_ID using ../temp/departmentList.dta, type(n:1) unmatched(none)
	local list "total_number_of_patients total_fee medical_test_fee inspection_fee treatment_fee surgery_fee sanitary_supplies_fee bed_fee nursing_fee total_drug_fee western_medicine_fee chinese_patent_medici chinese_herbal_medici other_fee"
	foreach v in `list' {
		local graphlist ""
		forvalues k = 1(1)9 {
			preserve
			keep if rankDepartment == `k'
			tw ///
			(scatter `v' d, mcolor(gs10) msize(tiny)) ///
			(lpolyci `v' d if d<0, lcolor(edkblue) lpattern(solid) fcolor(edkblue) fintensity(inten20) alcolor(white)) ///
			(lpolyci `v' d if d>0, lcolor(edkblue) lpattern(solid) fcolor(edkblue) fintensity(inten20) alcolor(white)), ///
			xline(0) legend(off) title("`v'") ///
			graphregion(fcolor(white)) name(`v'`k') 
			local graphlist "`graphlist' `v'`k'"
			restore
		}
		graph combine `graphlist', graphregion(fcolor(white))
		graph export ../output/figures/`v'byDepartment.eps, replace
		graph drop _all
	}
	local list "total_fee medical_test_fee inspection_fee treatment_fee surgery_fee sanitary_supplies_fee bed_fee nursing_fee total_drug_fee western_medicine_fee chinese_patent_medici chinese_herbal_medici other_fee"
	foreach v in `list' {
		local graphlist ""
		forvalues k = 1(1)9 {
			preserve
			keep if rankDepartment == `k'
			replace `v' = `v'/(total_number_of_patients+1)
			tw ///
			(scatter `v' d, mcolor(gs10) msize(tiny)) ///
			(lpolyci `v' d if d<0, lcolor(edkblue) lpattern(solid) fcolor(edkblue) fintensity(inten20) alcolor(white)) ///
			(lpolyci `v' d if d>0, lcolor(edkblue) lpattern(solid) fcolor(edkblue) fintensity(inten20) alcolor(white)), ///
			xline(0) legend(off) title("`v'") ///
			graphregion(fcolor(white)) name(`v'`k') 
			local graphlist "`graphlist' `v'`k'"
			restore
		}
		graph combine `graphlist', graphregion(fcolor(white))
		graph export ../output/figures/`v'_percapita_byDepartment.eps, replace
		graph drop _all
	}	
	local graphlist ""
	forvalues k = 1(1)16 {
			preserve
			keep if rankDepartment == `k'
			tw ///
			(scatter regisration_fee d, mcolor(gs10) msize(tiny)) ///
			(lpolyci regisration_fee d if d<0, lcolor(edkblue) lpattern(solid) fcolor(edkblue) fintensity(inten20) alcolor(white)) ///
			(lpolyci regisration_fee d if d>0, lcolor(edkblue) lpattern(solid) fcolor(edkblue) fintensity(inten20) alcolor(white)), ///
			xline(0) legend(off) title("`v'") ///
			graphregion(fcolor(white)) name(regisration_fee`k') 
			local graphlist "`graphlist' regisration_fee`k'"
			restore
		}
	graph combine `graphlist', graphregion(fcolor(white))
	graph export ../output/figures/regisration_feebyDepartment.eps, replace	
	graph drop _all
	
	local graphlist ""
	forvalues k = 1(1)6 {
			preserve
			keep if rankDepartment == `k'
			replace regisration_fee = regisration_fee/(total_number_of_patients+1)
			tw ///
			(scatter regisration_fee d, mcolor(gs10) msize(tiny)) ///
			(lpolyci regisration_fee d if d<0, lcolor(edkblue) lpattern(solid) fcolor(edkblue) fintensity(inten20) alcolor(white)) ///
			(lpolyci regisration_fee d if d>0, lcolor(edkblue) lpattern(solid) fcolor(edkblue) fintensity(inten20) alcolor(white)), ///
			xline(0) legend(off) title("`v'") ///
			graphregion(fcolor(white)) name(regisration_fee`k') 
			local graphlist "`graphlist' regisration_fee`k'"
			restore
		}
	graph combine `graphlist', graphregion(fcolor(white))
	graph export ../output/figures/regisration_fee_percapita_byDepartment.eps, replace	
	graph drop _all	
	
	replace regisration_fee = regisration_fee/(total_number_of_patients+1)
	drop if regisration_fee == 0
	tw ///
	(scatter regisration_fee d if d~=0, mcolor(gs10) msize(tiny)) ///
	(lpolyci regisration_fee d if d<0, lcolor(edkblue) lpattern(solid) fcolor(edkblue) fintensity(inten20) alcolor(white)) ///
	(lpolyci regisration_fee d if d>0, lcolor(edkblue) lpattern(solid) fcolor(edkblue) fintensity(inten20) alcolor(white)), ///
	xline(0) legend(off) title("`v'") ///
	graphregion(fcolor(white)) name(regisration_fee) 
	graph export ../output/figures/regisration_fee_percapita.eps, replace		
end


program figures_patientRevisit
	use ../external/patient_revisit_Tline.dta, clear
	local list "number_of_patients_in_outpatien number_of_readmission_patients number_of_booking_for_readmissio readmission_patient_rate readmission_booking_rate"
	collapse (sum) `list', by(year month timeLine)
	g d = timeLine - 31
	foreach v in `list' {
		tw ///
		(scatter `v' d, mcolor(gs10) msize(tiny)) ///
		(lpolyci `v' d if d<0, lcolor(edkblue) lpattern(solid) fcolor(edkblue) fintensity(inten20) alcolor(white)) ///
		(lpolyci `v' d if d>0, lcolor(edkblue) lpattern(solid) fcolor(edkblue) fintensity(inten20) alcolor(white)), ///
		xline(0) legend(off) title("`v'") ///
		graphregion(fcolor(white)) name(`v') 
		graph export ../output/figures/`v'_allDepartmentMean.eps, replace	
	}	
	graph drop _all	
end


program figures_profit_perCapita
	use ../external/profit_percapita_category_Tline.dta, clear
	rename (average_outpatient_prescription_ average_outpatient_expenses average_outpatient_drug_expenses average_outpatient_treatment_exp) ///
	       (percapita_prescriptionFee percapita_expenses percapita_drug_expenses percapita_treatmentFee)
	local list "percapita_prescriptionFee percapita_expenses percapita_drug_expenses percapita_treatmentFee"
	collapse (sum) `list', by(year month timeLine)
	g d = timeLine - 31
	foreach v in `list' {
		tw ///
		(scatter `v' d, mcolor(gs10) msize(tiny)) ///
		(lpolyci `v' d if d<-7, lcolor(edkblue) lpattern(solid) fcolor(edkblue) fintensity(inten20) alcolor(white)) ///
		(lpolyci `v' d if d>-7 & d<0, lcolor(edkblue) lpattern(solid) fcolor(edkblue) fintensity(inten20) alcolor(white)) ///
		(lpolyci `v' d if d>0, lcolor(edkblue) lpattern(solid) fcolor(edkblue) fintensity(inten20) alcolor(white)), ///
		xline(0) xline(-7) legend(off) title("`v'") ///
		graphregion(fcolor(white)) name(`v'_twoP) 
		graph export ../output/figures/`v'_allDepartmentMean_twoPolicy.eps, replace
		
		tw ///
		(scatter `v' d, mcolor(gs10) msize(tiny)) ///
		(lpolyci `v' d if d<0, lcolor(edkblue) lpattern(solid) fcolor(edkblue) fintensity(inten20) alcolor(white)) ///
		(lpolyci `v' d if d>0, lcolor(edkblue) lpattern(solid) fcolor(edkblue) fintensity(inten20) alcolor(white)), ///
		xline(0) legend(off) title("`v'") ///
		graphregion(fcolor(white)) name(`v') 
		graph export ../output/figures/`v'_allDepartmentMean.eps, replace	
	}
	graph drop _all
end



program figures_other
	use ../external/surgery_category_Tline.dta, clear
	rename (total_number_of_outpatient_surge) ///
	       (total_number_surgery)
	local list "total_number_surgery"
	collapse (sum) `list', by(year month timeLine)
	g d = timeLine - 31
	foreach v in `list' {
		tw ///
		(scatter `v' d, mcolor(gs10) msize(tiny)) ///
		(lpolyci `v' d if d<0, lcolor(edkblue) lpattern(solid) fcolor(edkblue) fintensity(inten20) alcolor(white)) ///
		(lpolyci `v' d if d>0, lcolor(edkblue) lpattern(solid) fcolor(edkblue) fintensity(inten20) alcolor(white)), ///
		xline(0) legend(off) title("`v'") ///
		graphregion(fcolor(white)) name(`v') 
		graph export ../output/figures/`v'_allDepartmentMean.eps, replace	
	}
	graph drop _all
	
	use ../external/prescription_category_Tline.dta, clear
	rename (number_of_outpatient_prescriptio C D E) ///
	       (prescription_chineseHerb prescription_chinesePatent prescription_westernMedicine prescription_antiMedicine)
	local list "prescription_chineseHerb prescription_chinesePatent prescription_westernMedicine prescription_antiMedicine"
	collapse (sum) `list', by(year month timeLine)
	g d = timeLine - 31
	foreach v in `list' {
		tw ///
		(scatter `v' d, mcolor(gs10) msize(tiny)) ///
		(lpolyci `v' d if d<0, lcolor(edkblue) lpattern(solid) fcolor(edkblue) fintensity(inten20) alcolor(white)) ///
		(lpolyci `v' d if d>0, lcolor(edkblue) lpattern(solid) fcolor(edkblue) fintensity(inten20) alcolor(white)), ///
		xline(0) legend(off) title("`v'") ///
		graphregion(fcolor(white)) name(`v') 
		graph export ../output/figures/`v'_allDepartmentMean.eps, replace	
	}
	graph drop _all	

	use ../external/drug_category_Tline.dta, clear
	rename (outpatient_western_medicine_fee outpatient_Chinese_patent_medici outpatient_Chinese_herbal_medici outpatient_antibacterial_medicin) ///
	       (western_medicine Chinese_patent_medici Chinese_herbal_medici antibacterial_medicine)
	local list "western_medicine Chinese_patent_medici Chinese_herbal_medici antibacterial_medicine"
	collapse (sum) `list', by(year month timeLine)
	g d = timeLine - 31
	foreach v in `list' {
		tw ///
		(scatter `v' d, mcolor(gs10) msize(tiny)) ///
		(lpolyci `v' d if d<0, lcolor(edkblue) lpattern(solid) fcolor(edkblue) fintensity(inten20) alcolor(white)) ///
		(lpolyci `v' d if d>0, lcolor(edkblue) lpattern(solid) fcolor(edkblue) fintensity(inten20) alcolor(white)), ///
		xline(0) legend(off) title("`v'") ///
		graphregion(fcolor(white)) name(`v') 
		graph export ../output/figures/`v'_allDepartmentMean.eps, replace	
	}
	graph drop _all		
/*	
	use ../external/prescription_category_Tline.dta, clear
	rename (number_of_outpatient_prescriptio C D E) ///
	       (prescription_chineseHerb prescription_chinesePatent prescription_westernMedicine prescription_antiMedicine)
    mmerge department_ID using ../temp/departmentList.dta, type(n:1) unmatched(none)
	g d = timeLine - 31
	local list "prescription_chineseHerb prescription_chinesePatent prescription_westernMedicine prescription_antiMedicine"
	foreach v in `list' {
		local graphlist ""
		forvalues k = 1(1)9 {
			preserve
			keep if rankDepartment == `k'
			tw ///
			(scatter `v' d, mcolor(gs10) msize(tiny)) ///
			(lpolyci `v' d if d<0, lcolor(edkblue) lpattern(solid) fcolor(edkblue) fintensity(inten20) alcolor(white)) ///
			(lpolyci `v' d if d>0, lcolor(edkblue) lpattern(solid) fcolor(edkblue) fintensity(inten20) alcolor(white)), ///
			xline(0) legend(off) title("`v'") ///
			graphregion(fcolor(white)) name(`v'`k') 
			local graphlist "`graphlist' `v'`k'"
			restore
		}
		graph combine `graphlist', graphregion(fcolor(white))
		graph export ../output/figures/`v'_byDepartment.eps, replace
		graph drop _all
	}
*/
end
 

main


