REM ****************************************************
REM * make.bat: double-click to run all scripts
REM ****************************************************

REM ******************************************************
REM * Manually copy-paste the raw data files into external
REM ******************************************************

SET LOG=..\output\make.log

REM DELETE OUTPUT & TEMP FILES
DEL /F /Q ..\output\
RMDIR ..\temp /S /Q
mkdir ..\temp
RMDIR ..\output /S /Q
mkdir ..\output
RMDIR ..\output\tables /S /Q
mkdir ..\output\tables
RMDIR ..\output\figures /S /Q
mkdir ..\output\figures


REM LOG START
ECHO make.bat started	>%LOG%
ECHO %DATE%		>>%LOG%
ECHO %TIME%		>>%LOG%
dir ..\output\ >>%LOG%

%STATAEXE% /e do tables.do
%STATAEXE% /e do figures.do

COPY %LOG%+tables.log+figures.log %LOG%
 
DEL tables.log figures.log

REM CLOSE LOG
ECHO make.bat completed	>>%LOG%
ECHO %DATE%		>>%LOG%
ECHO %TIME%		>>%LOG%

PAUSE
