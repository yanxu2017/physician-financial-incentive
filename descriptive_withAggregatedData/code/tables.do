cap log close
set linesize 250
version 12

clear all
set matsize 5000
set more off, permanently

set seed 66666666
set sortseed 66666666
set scheme s2mono

adopath + ..\external


program main
	tables_departmentSize
end

 
program tables_departmentSize
	use ../external/profit_category_Tline.dta, clear
	rename (total_number_of_patients_in_outp total_outpatient_fee outpatient_regisration_fee outpatient_examination_fee_fee outpatient_Medical_test_fee outpatient_inspection_fee outpatient_treatment_fee outpatient_surgery_fee outpatient_sanitary_supplies_fee outpatient_bed_fee outpatient_nursing_fee total_outpatient_drug_fee outpatient_western_medicine_fee outpatient_Chinese_patent_medici outpatient_Chinese_herbal_medici outpatient_other_fee) ///
		   (total_number_of_patients total_fee regisration_fee examination_fee_fee medical_test_fee inspection_fee treatment_fee surgery_fee sanitary_supplies_fee bed_fee nursing_fee total_drug_fee western_medicine_fee chinese_patent_medici chinese_herbal_medici other_fee)	
	local list "total_number_of_patients total_fee regisration_fee examination_fee_fee medical_test_fee inspection_fee treatment_fee surgery_fee sanitary_supplies_fee bed_fee nursing_fee total_drug_fee western_medicine_fee chinese_patent_medici chinese_herbal_medici other_fee"
	collapse (sum) total_fee total_number_of_patients total_drug_fee, by(department_ID subdepartment_ID)
	mmerge department_ID subdepartment_ID using ../external/department_subdepartment.dta, type(1:1)
	g department_fillin = department
	replace department_fillin = subdepartment if department_fillin == ""
	collapse (sum) total_fee total_number_of_patients total_drug_fee, by(department_fillin department department_ID)
	gsort -total_fee
	drop if total_number_of_patients == 0
	g fee_per_patient = total_fee/total_number_of_patients
	save ../output/departmentSize.dta, replace
	
	drop if department == ""
	gsort -total_number_of_patients
	g rankDepartment = _n
	keep department total_fee total_number_of_patients total_drug_fee fee_per_patient department_ID rankDepartment
	export excel using "..\output\departmentListRankedBySize.xls", firstrow(variables) replace
	*focus on the departments that have more than 100,000 patients in two years
	drop if total_number_of_patients < 100000
	save ../temp/departmentList.dta, replace
end


main


