#LyX 2.3 created this file. For more info see http://www.lyx.org/
\lyxformat 544
\begin_document
\begin_header
\save_transient_properties true
\origin unavailable
\textclass article
\use_default_options true
\maintain_unincluded_children false
\language english
\language_package default
\inputencoding utf8-plain
\fontencoding global
\font_roman "times" "default"
\font_sans "default" "default"
\font_typewriter "default" "default"
\font_math "auto" "auto"
\font_default_family rmdefault
\use_non_tex_fonts false
\font_sc false
\font_osf false
\font_sf_scale 100 100
\font_tt_scale 100 100
\use_microtype false
\use_dash_ligatures true
\graphics default
\default_output_format default
\output_sync 0
\bibtex_command default
\index_command default
\paperfontsize default
\spacing single
\use_hyperref false
\papersize default
\use_geometry true
\use_package amsmath 1
\use_package amssymb 1
\use_package cancel 1
\use_package esint 1
\use_package mathdots 1
\use_package mathtools 1
\use_package mhchem 1
\use_package stackrel 1
\use_package stmaryrd 1
\use_package undertilde 1
\cite_engine natbib
\cite_engine_type authoryear
\biblio_style plainnat
\use_bibtopic false
\use_indices false
\paperorientation portrait
\suppress_date false
\justification true
\use_refstyle 1
\use_minted 0
\index Index
\shortcut idx
\color #008000
\end_index
\leftmargin 1in
\topmargin 1in
\rightmargin 1in
\bottommargin 1in
\secnumdepth 3
\tocdepth 3
\paragraph_separation indent
\paragraph_indentation default
\is_math_indent 0
\math_numbering_side default
\quotes_style english
\dynamic_quotes 0
\papercolumns 1
\papersides 1
\paperpagestyle default
\tracking_changes false
\output_changes false
\html_math_output 0
\html_css_as_file 0
\html_be_strict false
\end_header

\begin_body

\begin_layout Subsection*
1.
 This project
\end_layout

\begin_layout Standard
\begin_inset Note Note
status collapsed

\begin_layout Plain Layout
It is well documented that the level and growth of the US health-care sector
 is high relative to any other developed country, and that this higher spending
 is not associated with better health outcomes.
 Economists and policymakers frequently attribute these facts to idiosyncratic
 institutional features of the US health-care sector, focusing on generous
 health insurance coverage that insulates consumers from the direct financial
 consequences of their healthcare consumption decisions, and public sector
 reimbursement and regulation that provides little incentive for providers
 to engage in efficient production (Weisbrod 1991; Fuchs 2014).
 Such features have been suggested to be the cause of what makes the American
 health-care system, in the words of Alan Garber and Jonathan Skinner, “uniquely
 inefficient” (Garber and Skinner 2008).
\end_layout

\end_inset


\begin_inset Note Note
status collapsed

\begin_layout Plain Layout
Informed choice is when a person is given options to choose from several
 diagnostic tests or treatments, knowing the details, benefits, risks and
 expected outcome of each.
 Informed consent is when a person agrees to the test or treatment they
 have been offered, knowing the details, benefits, risks and expected outcome.
\end_layout

\end_inset


\end_layout

\begin_layout Itemize
Motivation
\end_layout

\begin_deeper
\begin_layout Itemize
The United States will see a shortage of up to nearly 122,000 physicians
 by 2032 as demand for physicians continues to grow faster than supply,
 according to the AAMC (Association of American Medical Colleges).
 
\end_layout

\begin_layout Itemize
Patient cost sharing (e.g., third-party payers in U.S.
 and government mandatory insurance in Japan) is one source that drives
 households' overuse of healthcare.
 
\end_layout

\end_deeper
\begin_layout Itemize
Research question
\end_layout

\begin_deeper
\begin_layout Itemize
to what extent patient cost affects household's healthcare usage behavior
\end_layout

\begin_deeper
\begin_layout Itemize
intensive margin: patient's choice between chief physicians (more scarce
 and more expensive healthcare resource) and general physicians (less scarce
 and less expensive healthcare resource)
\end_layout

\begin_layout Itemize
extensive margin: patinet's choice of visiting hospital
\end_layout

\end_deeper
\begin_layout Itemize
does patient cost change effectively discourage the low-need households
 to occupy healthcare resource? 
\end_layout

\begin_layout Itemize
to what extent an optimal registration fee structure can help physicians
 optimally allocate their time and resource.
 
\end_layout

\end_deeper
\begin_layout Itemize
Context
\end_layout

\begin_deeper
\begin_layout Itemize
during our data window, two policy events took place
\end_layout

\begin_deeper
\begin_layout Itemize
medicine add-on fee is banned by the government.
 The agency problem (physicians overuse or mis-use healthcare due to profit
 from drug) is not likely to drive the physicians' behavior or outcomes
 of patients in our data.
 
\end_layout

\begin_layout Itemize
half an year after removing medicine add-on fee, government launched a policy
 adjusting registration fee.
\end_layout

\end_deeper
\begin_layout Itemize
detailed panel data which allows us to track each patient at least for three
 years, observe diagnosis decisions, prescription decisions, doctor-patient
 matches, payments, treatment record, etc.
\end_layout

\begin_layout Itemize
business insurance is rare in China (need to check the percentage of households
 who have it).
\end_layout

\end_deeper
\begin_layout Itemize
Relation to the literature
\end_layout

\begin_deeper
\begin_layout Itemize
the most closely related paper is Shigeoka (2014).
\end_layout

\end_deeper
\begin_layout Subsection*
2.
 Figures
\end_layout

\begin_layout Standard
\begin_inset Float figure
placement H
wide false
sideways false
status open

\begin_layout Plain Layout
\align center
\begin_inset Caption Standard

\begin_layout Plain Layout

\end_layout

\end_inset


\begin_inset VSpace bigskip
\end_inset


\begin_inset Tabular
<lyxtabular version="3" rows="1" columns="1">
<features tabularvalignment="middle">
<column alignment="center" valignment="top">
<row>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\series bold
\begin_inset Graphics
	filename externals/figures/totalPatient_dermatology.eps
	scale 95

\end_inset


\end_layout

\end_inset
</cell>
</row>
</lyxtabular>

\end_inset


\end_layout

\begin_layout Plain Layout
\align center
\begin_inset VSpace defskip
\end_inset


\end_layout

\begin_layout Plain Layout
\align center
\begin_inset Box Frameless
position "t"
hor_pos "c"
has_inner_box 1
inner_pos "t"
use_parbox 0
use_makebox 0
width "98text%"
special "none"
height "1in"
height_special "totalheight"
thickness "0.4pt"
separation "3pt"
shadowsize "4pt"
framecolor "black"
backgroundcolor "none"
status open

\begin_layout Plain Layout
\paragraph_spacing other 0.8

\size footnotesize
Note: Plot registration fee paid by each patient.
 0 point is July 2018.
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Float figure
placement H
wide false
sideways false
status open

\begin_layout Plain Layout
\align center
\begin_inset Caption Standard

\begin_layout Plain Layout

\end_layout

\end_inset


\begin_inset VSpace bigskip
\end_inset


\begin_inset Tabular
<lyxtabular version="3" rows="1" columns="1">
<features tabularvalignment="middle">
<column alignment="center" valignment="top">
<row>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\series bold
\begin_inset Graphics
	filename externals/figures/uniqueTotalPatient_dermatology.eps
	scale 95

\end_inset


\end_layout

\end_inset
</cell>
</row>
</lyxtabular>

\end_inset


\end_layout

\begin_layout Plain Layout
\align center
\begin_inset VSpace defskip
\end_inset


\end_layout

\begin_layout Plain Layout
\align center
\begin_inset Box Frameless
position "t"
hor_pos "c"
has_inner_box 1
inner_pos "t"
use_parbox 0
use_makebox 0
width "98text%"
special "none"
height "1in"
height_special "totalheight"
thickness "0.4pt"
separation "3pt"
shadowsize "4pt"
framecolor "black"
backgroundcolor "none"
status open

\begin_layout Plain Layout
\paragraph_spacing other 0.8

\size footnotesize
Note: Plot registration fee paid by each patient.
 0 point is July 2018.
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Float figure
placement H
wide false
sideways false
status open

\begin_layout Plain Layout
\align center
\begin_inset Caption Standard

\begin_layout Plain Layout

\end_layout

\end_inset


\begin_inset VSpace bigskip
\end_inset


\begin_inset Tabular
<lyxtabular version="3" rows="1" columns="1">
<features tabularvalignment="middle">
<column alignment="center" valignment="top">
<row>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\series bold
\begin_inset Graphics
	filename externals/figures/totalPatient_rectum.eps
	scale 95

\end_inset


\end_layout

\end_inset
</cell>
</row>
</lyxtabular>

\end_inset


\end_layout

\begin_layout Plain Layout
\align center
\begin_inset VSpace defskip
\end_inset


\end_layout

\begin_layout Plain Layout
\align center
\begin_inset Box Frameless
position "t"
hor_pos "c"
has_inner_box 1
inner_pos "t"
use_parbox 0
use_makebox 0
width "98text%"
special "none"
height "1in"
height_special "totalheight"
thickness "0.4pt"
separation "3pt"
shadowsize "4pt"
framecolor "black"
backgroundcolor "none"
status open

\begin_layout Plain Layout
\paragraph_spacing other 0.8

\size footnotesize
Note: Plot registration fee paid by each patient by department for top 6
 departments.
 0 point is July 2018.
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Float figure
placement H
wide false
sideways false
status open

\begin_layout Plain Layout
\align center
\begin_inset Caption Standard

\begin_layout Plain Layout

\end_layout

\end_inset


\begin_inset VSpace bigskip
\end_inset


\begin_inset Tabular
<lyxtabular version="3" rows="1" columns="1">
<features tabularvalignment="middle">
<column alignment="center" valignment="top">
<row>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\series bold
\begin_inset Graphics
	filename externals/figures/uniqueTotalPatient_rectum.eps
	scale 95

\end_inset


\end_layout

\end_inset
</cell>
</row>
</lyxtabular>

\end_inset


\end_layout

\begin_layout Plain Layout
\align center
\begin_inset VSpace defskip
\end_inset


\end_layout

\begin_layout Plain Layout
\align center
\begin_inset Box Frameless
position "t"
hor_pos "c"
has_inner_box 1
inner_pos "t"
use_parbox 0
use_makebox 0
width "98text%"
special "none"
height "1in"
height_special "totalheight"
thickness "0.4pt"
separation "3pt"
shadowsize "4pt"
framecolor "black"
backgroundcolor "none"
status open

\begin_layout Plain Layout
\paragraph_spacing other 0.8

\size footnotesize
Note: Plot registration fee paid by each patient by department for top 6
 departments.
 0 point is July 2018.
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Float figure
placement H
wide false
sideways false
status open

\begin_layout Plain Layout
\align center
\begin_inset Caption Standard

\begin_layout Plain Layout

\end_layout

\end_inset


\begin_inset VSpace bigskip
\end_inset


\begin_inset Tabular
<lyxtabular version="3" rows="1" columns="1">
<features tabularvalignment="middle">
<column alignment="center" valignment="top">
<row>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\series bold
\begin_inset Graphics
	filename externals/figures/totalPatient_ear.eps
	scale 95

\end_inset


\end_layout

\end_inset
</cell>
</row>
</lyxtabular>

\end_inset


\end_layout

\begin_layout Plain Layout
\align center
\begin_inset VSpace defskip
\end_inset


\end_layout

\begin_layout Plain Layout
\align center
\begin_inset Box Frameless
position "t"
hor_pos "c"
has_inner_box 1
inner_pos "t"
use_parbox 0
use_makebox 0
width "98text%"
special "none"
height "1in"
height_special "totalheight"
thickness "0.4pt"
separation "3pt"
shadowsize "4pt"
framecolor "black"
backgroundcolor "none"
status open

\begin_layout Plain Layout
\paragraph_spacing other 0.8

\size footnotesize
Note: Plot the total number of patients for top 6 departments.
 0 point is July 2018.
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Float figure
placement H
wide false
sideways false
status open

\begin_layout Plain Layout
\align center
\begin_inset Caption Standard

\begin_layout Plain Layout

\end_layout

\end_inset


\begin_inset VSpace bigskip
\end_inset


\begin_inset Tabular
<lyxtabular version="3" rows="1" columns="1">
<features tabularvalignment="middle">
<column alignment="center" valignment="top">
<row>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\series bold
\begin_inset Graphics
	filename externals/figures/uniqueTotalPatient_ear.eps
	scale 95

\end_inset


\end_layout

\end_inset
</cell>
</row>
</lyxtabular>

\end_inset


\end_layout

\begin_layout Plain Layout
\align center
\begin_inset VSpace defskip
\end_inset


\end_layout

\begin_layout Plain Layout
\align center
\begin_inset Box Frameless
position "t"
hor_pos "c"
has_inner_box 1
inner_pos "t"
use_parbox 0
use_makebox 0
width "98text%"
special "none"
height "1in"
height_special "totalheight"
thickness "0.4pt"
separation "3pt"
shadowsize "4pt"
framecolor "black"
backgroundcolor "none"
status open

\begin_layout Plain Layout
\paragraph_spacing other 0.8

\size footnotesize
Note: Plot the total number of patients for top 6 departments.
 0 point is July 2018.
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Float figure
placement H
wide false
sideways false
status open

\begin_layout Plain Layout
\align center
\begin_inset Caption Standard

\begin_layout Plain Layout

\end_layout

\end_inset


\begin_inset VSpace bigskip
\end_inset


\begin_inset Tabular
<lyxtabular version="3" rows="1" columns="1">
<features tabularvalignment="middle">
<column alignment="center" valignment="top">
<row>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\series bold
\begin_inset Graphics
	filename externals/figures/fee_dermatology.eps
	scale 120

\end_inset


\end_layout

\end_inset
</cell>
</row>
</lyxtabular>

\end_inset


\end_layout

\begin_layout Plain Layout
\align center
\begin_inset VSpace defskip
\end_inset


\end_layout

\begin_layout Plain Layout
\align center
\begin_inset Box Frameless
position "t"
hor_pos "c"
has_inner_box 1
inner_pos "t"
use_parbox 0
use_makebox 0
width "98text%"
special "none"
height "1in"
height_special "totalheight"
thickness "0.4pt"
separation "3pt"
shadowsize "4pt"
framecolor "black"
backgroundcolor "none"
status open

\begin_layout Plain Layout
\paragraph_spacing other 0.8

\size footnotesize
Note: 
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Float figure
placement H
wide false
sideways false
status open

\begin_layout Plain Layout
\align center
\begin_inset Caption Standard

\begin_layout Plain Layout

\end_layout

\end_inset


\begin_inset VSpace bigskip
\end_inset


\begin_inset Tabular
<lyxtabular version="3" rows="1" columns="1">
<features tabularvalignment="middle">
<column alignment="center" valignment="top">
<row>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\series bold
\begin_inset Graphics
	filename externals/figures/fee_rectum.eps
	scale 95

\end_inset


\end_layout

\end_inset
</cell>
</row>
</lyxtabular>

\end_inset


\end_layout

\begin_layout Plain Layout
\align center
\begin_inset VSpace defskip
\end_inset


\end_layout

\begin_layout Plain Layout
\align center
\begin_inset Box Frameless
position "t"
hor_pos "c"
has_inner_box 1
inner_pos "t"
use_parbox 0
use_makebox 0
width "98text%"
special "none"
height "1in"
height_special "totalheight"
thickness "0.4pt"
separation "3pt"
shadowsize "4pt"
framecolor "black"
backgroundcolor "none"
status open

\begin_layout Plain Layout
\paragraph_spacing other 0.8

\size footnotesize
Note: Plot the total number of patients for top 6 departments.
 0 point is July 2018.
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Float figure
placement H
wide false
sideways false
status open

\begin_layout Plain Layout
\align center
\begin_inset Caption Standard

\begin_layout Plain Layout

\end_layout

\end_inset


\begin_inset VSpace bigskip
\end_inset


\begin_inset Tabular
<lyxtabular version="3" rows="1" columns="1">
<features tabularvalignment="middle">
<column alignment="center" valignment="top">
<row>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\series bold
\begin_inset Graphics
	filename externals/figures/fee_ear.eps
	scale 95

\end_inset


\end_layout

\end_inset
</cell>
</row>
</lyxtabular>

\end_inset


\end_layout

\begin_layout Plain Layout
\align center
\begin_inset VSpace defskip
\end_inset


\end_layout

\begin_layout Plain Layout
\align center
\begin_inset Box Frameless
position "t"
hor_pos "c"
has_inner_box 1
inner_pos "t"
use_parbox 0
use_makebox 0
width "98text%"
special "none"
height "1in"
height_special "totalheight"
thickness "0.4pt"
separation "3pt"
shadowsize "4pt"
framecolor "black"
backgroundcolor "none"
status open

\begin_layout Plain Layout
\paragraph_spacing other 0.8

\size footnotesize
Note: Plot the total amount of fee paid by each patient in a given month.
 0 point is Dec 2017.
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Subsection*
2.
 Policy changes 
\end_layout

\begin_layout Itemize
Policy
\end_layout

\begin_deeper
\begin_layout Itemize
policy is at province level, made by the government
\end_layout

\begin_layout Itemize
After 2017 December, the hospitals are required to remove the 
\begin_inset Quotes eld
\end_inset

drug add rate
\begin_inset Quotes erd
\end_inset

 from all the drugs.
 Hospital were able to charge 15% drug add rate on all the drugs doctors
 prescribe.
 This 15% markup was removed after this policy.
 Hospitals and doctors cannot make profit through prescribe drug anymore.
\end_layout

\begin_layout Itemize
2018 July, increase the registration fee for all three types of physicians:
 general physician, associate 
\begin_inset Float table
placement H
wide false
sideways false
status open

\begin_layout Plain Layout
\begin_inset Caption Standard

\begin_layout Plain Layout
Registration fee 
\begin_inset CommandInset label
LatexCommand label
name "tab:registrationFee"

\end_inset


\end_layout

\end_inset


\begin_inset VSpace bigskip
\end_inset


\end_layout

\begin_layout Plain Layout
\align center
\begin_inset Tabular
<lyxtabular version="3" rows="5" columns="4">
<features tabularvalignment="middle">
<column alignment="left" valignment="top" width="0pt">
<column alignment="center" valignment="top" width="0pt">
<column alignment="center" valignment="top" width="0pt">
<column alignment="center" valignment="top" width="0pt">
<row>
<cell alignment="left" valignment="top" topline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
<cell multicolumn="1" alignment="center" valignment="top" topline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
registration fee paid by the patient
\end_layout

\end_inset
</cell>
<cell multicolumn="2" alignment="center" valignment="top" topline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
<cell multicolumn="2" alignment="center" valignment="top" topline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" bottomline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
physician type
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" bottomline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
prior to 2017-12
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" bottomline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
2017-12 to 2018-07
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" bottomline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
after 2018-07
\end_layout

\end_inset
</cell>
</row>
<row interlinespace="3pt">
<cell alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
general physician
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
3
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
3
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
1
\end_layout

\end_inset
</cell>
</row>
<row interlinespace="3pt">
<cell alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
associate chief physician
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
5
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
5
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
6
\end_layout

\end_inset
</cell>
</row>
<row interlinespace="3pt">
<cell alignment="left" valignment="top" bottomline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
chief physician
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" bottomline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
8
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" bottomline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
8
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" bottomline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
16
\end_layout

\end_inset
</cell>
</row>
</lyxtabular>

\end_inset


\begin_inset VSpace defskip
\end_inset


\end_layout

\begin_layout Plain Layout
\align center
\begin_inset Tabular
<lyxtabular version="3" rows="5" columns="4">
<features tabularvalignment="middle">
<column alignment="left" valignment="top" width="0pt">
<column alignment="center" valignment="top" width="0pt">
<column alignment="center" valignment="top" width="0pt">
<column alignment="center" valignment="top" width="0pt">
<row>
<cell alignment="left" valignment="top" topline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
<cell multicolumn="1" alignment="center" valignment="top" topline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
registration fee paid by the government
\end_layout

\end_inset
</cell>
<cell multicolumn="2" alignment="center" valignment="top" topline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
<cell multicolumn="2" alignment="center" valignment="top" topline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" bottomline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
physician type
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" bottomline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
prior to 2017-12
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" bottomline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
2017-12 to 2018-07
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" bottomline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
after 2018-07
\end_layout

\end_inset
</cell>
</row>
<row interlinespace="3pt">
<cell alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
general physician
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
0
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
0
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
14
\end_layout

\end_inset
</cell>
</row>
<row interlinespace="3pt">
<cell alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
associate chief physician
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
0
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
0
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
14
\end_layout

\end_inset
</cell>
</row>
<row interlinespace="3pt">
<cell alignment="left" valignment="top" bottomline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
chief physician
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" bottomline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
0
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" bottomline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
0
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" bottomline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
14
\end_layout

\end_inset
</cell>
</row>
</lyxtabular>

\end_inset


\end_layout

\begin_layout Plain Layout
\align center
\begin_inset VSpace defskip
\end_inset


\end_layout

\begin_layout Plain Layout
\align center
\begin_inset Box Frameless
position "t"
hor_pos "c"
has_inner_box 1
inner_pos "t"
use_parbox 0
use_makebox 0
width "82text%"
special "none"
height "1in"
height_special "totalheight"
thickness "0.4pt"
separation "3pt"
shadowsize "4pt"
framecolor "black"
backgroundcolor "none"
status open

\begin_layout Plain Layout
\paragraph_spacing other 0.8

\size footnotesize
Note: The registration fee is reported in RMB.
 It refers to the registration fee a patient needs to pay if he/she wants
 to have an appointment with the doctor.
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\end_deeper
\begin_layout Itemize
Why government wants to change the payment structure?
\end_layout

\begin_deeper
\begin_layout Itemize
decrease the hospital's profit from drug and increase the profit from the
 service fee on each patient
\end_layout

\begin_layout Itemize
reward the physicians with high human capital.
\end_layout

\end_deeper
\end_body
\end_document
